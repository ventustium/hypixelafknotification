<?php

namespace App\Http\Controllers;

use App\Mail\AFKEmail;
use App\Mail\NotAFKEmail;
use App\Mail\SBNotification;
use App\Models\HypixelSB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Exception;

class HypixelSBController extends Controller
{
    //

    public function readlog () {
        $log = File::get(storage_path('app/minecraft_logs/latest.log'));
        $array = explode("\n", $log);
        $last10 = "";

        for ($i = max(0, count($array)-21); $i < count($array); $i++) {
            $last10 .= $array[$i] . "<br>";
        }

        echo($last10);
        $status = HypixelSB::latest('id')->first();

        if(!str_contains($last10, "Your Witherborn hit") || str_contains($last10, "Detected leaving skyblock!")) {
            if($status == null || $status->afk == 1) {
                $status = HypixelSB::create([
                    'afk' => 0,
                    'email_send' => 0
                ]);
                try{
                    Mail::to('kevinlinuhung@gmail.com')->send(new NotAFKEmail($last10));
                    $status->email_send = 1;
                    $status->save();
                }
                catch (Exception $e){
                    echo $e;
                    return 0;
                }
            }
        }

        else if (str_contains($last10, "Your Witherborn hit")) {
            if($status == null || $status->afk == 0) {
                $status = HypixelSB::create([
                    'afk' => 1,
                    'email_send' => 0
                ]);
                try{
                    Mail::to('kevinlinuhung@gmail.com')->send(new NotAFKEmail($last10));
                    $status->email_send = 1;
                    $status->save();
                }
                catch (Exception $e){
                    echo $e;
                    return 0;
                }
            }
        }
    }

    public function testlog(){
        echo ('Hourly called - ');
        $log = File::get(storage_path('app/minecraft_logs/latest.log'));
        $array = explode("\n", $log);
        $last10 = "";

        for ($i = max(0, count($array)-21); $i < count($array); $i++) {
            $last10 .= $array[$i] . "<br>";
        }

        $status = HypixelSB::latest('id')->first();
        if($status->afk == 1) {

            try{
                echo ('Sending Mail - ');
                Mail::to('kevinlinuhung@gmail.com')->send(new AFKEmail($last10));
                echo ('Send Mail Success');
            }
            catch(Exception $e) {
                echo ('Send Mail Error: ' . $e);
            }
        }

        else if($status->afk == 0) {

            try{
                echo ('Sending Mail - ');
                Mail::to('kevinlinuhung@gmail.com')->send(new NotAFKEmail($last10));
                echo ('Send Mail Success');
            }
            catch(Exception $e) {
                echo ('Send Mail Error: ' . $e);
            }
        }
    }

    public function testmail(){
        $last10 = 'TESTING ONLY';

        try{
            echo ('Sending Mail - ');
            Mail::to('kevinlinuhung@gmail.com')->send(new AFKEmail($last10));
            echo ('Send Mail Success');
        }
        catch(Exception $e) {
            echo ('Send Mail Error: ' . $e);
        }
    }
}
