<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HypixelSB extends Model
{
    use HasFactory;

    protected $fillable = [
        'email_send', 'afk'
    ];
}
