<?php

namespace App\Console;

use App\Mail\AFKEmail;
use App\Mail\NotAFKEmail;
use App\Models\HypixelSB;
use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->call(function(){
            echo('Minutes Called ');
            $log = File::get(storage_path('app/minecraft_logs/latest.log'));
            $array = explode("\n", $log);
            $last10 = "";

            for ($i = max(0, count($array)-11); $i < count($array); $i++) {
                $last10 .= $array[$i] . "<br>";
            }

            $status = HypixelSB::latest('id')->first();

            if(!str_contains($last10, "Your Witherborn hit") || str_contains($last10, "Detected leaving skyblock!")) {
                if($status == null || $status->afk == 1) {
                    $status = HypixelSB::create([
                        'afk' => 0,
                        'email_send' => 0
                    ]);
                    try{
                        Mail::to('kevinlinuhung@gmail.com')->send(new NotAFKEmail($last10));
                        $status->email_send = 1;
                        $status->save();
                        echo ('Send Mail Success');
                    }
                    catch(Exception $e) {
                        echo ('Send Mail Error: ' . $e);
                        return 0;
                    }
                }
            }

            else if (str_contains($last10, "Your Witherborn hit")) {
                if($status == null || $status->afk == 0) {
                    $status = HypixelSB::create([
                        'afk' => 1,
                        'email_send' => 0
                    ]);

                    try{
                        Mail::to('kevinlinuhung@gmail.com')->send(new AFKEmail($last10));
                        $status->email_send = 1;
                        $status->save();
                        echo ('Send Mail Success');
                    }
                    catch(Exception $e) {
                        echo ('Send Mail Error: ' . $e);
                        return 0;
                    }
                }
            }
        })->everyMinute();

        $schedule->call(function(){
            echo ('Hourly called ');
            $log = File::get(storage_path('app/minecraft_logs/latest.log'));
            $array = explode("\n", $log);
            $last10 = "";

            for ($i = max(0, count($array)-11); $i < count($array); $i++) {
                $last10 .= $array[$i] . "<br>";
            }

            echo ('Hourly called');
            $status = HypixelSB::latest('id')->first();
            if($status == null || $status->afk == 1) {

                try{
                    Mail::to('kevinlinuhung@gmail.com')->send(new AFKEmail($last10));
                    echo ('Send Mail Success');
                }
                catch(Exception $e) {
                    echo ('Send Mail Error: ' . $e);
                    return 0;
                }
            }
        })->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
