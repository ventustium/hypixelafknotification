# MC Hypixel Skyblock Notification AFK
Did you like AFK on Hypixel Skyblock?

I like AFK on Hypixel Skyblock, so I made this System using Laravel

Well as you all know, Laravel is too powerful for this System, but I have another plan... I will implement this System on my main API System which is using Laravel

## How this system works
Well basicly this system will read your latest minecraft log. And detect if you're still killing mobs using witherborn.


## How to use / install
Make sure you already installed composser, PHP 8, and database server

And then install the system on your PC
```
git clone git@gitlab.com:ventustium/hypixelafknotification.git
cd HypixelAFKNotification/
composer install
composer dump-autoload
```
Create a symlink for your minecraft latest log
```
cd storage/app/
ls -n ~/.minecraft/logs minecraft_logs
```
Copy .env.example

Fill the required data like your database, mail server, etc
```
cp .env.example .env
php artisan key:generate
```

Create cron (launch the scheduler)
```
crontab -e
* * * * * cd ~/HypixelAFKNotification && php artisan schedule:run >> /home/kevin/logs/notification.txt
```
