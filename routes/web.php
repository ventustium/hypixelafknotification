<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HypixelSBController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/minecraft/log/read/', [HypixelSBController::class, 'readlog']);
Route::get('/minecraft/log/test/', [HypixelSBController::class, 'testlog']);
Route::get('/minecraft/log/testmail/', [HypixelSBController::class, 'testmail']);
